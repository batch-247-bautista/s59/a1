import { Card, Row, Col } from 'react-bootstrap';

export default function Highlight() {
  return (
    <Row className="mt-3 mb-3">
        <Col xs={12} md={4}>
            <Card className="cardHighlight p-3">
                <Card.Body>
                    <Card.Title>
                        <h2>Order Anywhere</h2>
                    </Card.Title>
                    <Card.Text>
                        You can place on your phone, computer and any of Shoes Mio branches nationwide!
                    </Card.Text>
                </Card.Body>
            </Card>
        </Col>
        <Col xs={12} md={4}>
            <Card className="cardHighlight p-3">
                <Card.Body>
                    <Card.Title>
                        <h2>Order Now, Before stock Runs Out</h2>
                    </Card.Title>
                    <Card.Text>
                        Orders and purchases are subject to stock availability.  Confirmation of stock will be determined upon checkout.
                    </Card.Text>
                </Card.Body>
            </Card>
        </Col>
        <Col xs={12} md={4}>
            <Card className="cardHighlight p-3">
                <Card.Body>
                    <Card.Title>
                        <h2>Be Stylish. Get the latest trend in shoes!</h2>
                    </Card.Title>
                    <Card.Text>
                        Don't be left out on the what's new in shoes.  We have a line of lifestyle and active wear brands and models for your choosing.
                    </Card.Text>
                </Card.Body>
            </Card>
        </Col>
    </Row>
  );
}

const ProductCard = ({ product }) => {
  return (
    <Card className="product-card p-3">
      <Card.Img variant="top" src={product.image} />
      <Card.Body>
        <Card.Title>{product.title}</Card.Title>
        <Card.Text>{product.description}</Card.Text>
        <Card.Text>Price: ${product.price}</Card.Text>
        <button className="btn btn-primary">Enroll now</button>
      </Card.Body>
    </Card>
  );
};

