

import { Card, Button } from 'react-bootstrap';

import PropTypes from 'prop-types';
 import {Link} from 'react-router-dom';


export default function ProductCard({product}){



const {name, description, price, _id} = product;



return (
  <Card className= "my-3">
    <Card.Body>
      <Card.Title>{name}</Card.Title>
      <Card.Subtitle>Description</Card.Subtitle>
      <Card.Text>{description}</Card.Text>
      <Card.Subtitle>Price</Card.Subtitle>
      <Card.Text>{price}</Card.Text>
      <Button className="bg-primary" as={Link} to={`/products/${_id}`}>Details</Button>
    </Card.Body>
  </Card>
  )
}



  

ProductCard.propTypes = {
  product: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired
  })
}