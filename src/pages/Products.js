//import coursesData from '../data/productsData';
import ProductCard from '../components/ProductCard';
import Loading from '../components/Loading';

import { useState, useEffect } from 'react';

export default function Products() {
	
	
	const [ products, setProducts ] = useState([]);
	const [ isLoading, setIsLoading] = useState(true);


	// Retrieves the products from the database upon initial render of the "Products" component.
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/active`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			// Sets the "products" state to map the data retrived from the fetch request into several "ProductCard" components
			setProducts(data.map(product => {
				return(
					<ProductCard key={product._id} product={product} />
				)
			}))

			setIsLoading(false);
		})
	}, [])

	return(
		(isLoading) ?
			<Loading />
		:
		<>
			{products}
		</>
	)

}
