import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';

import { useNavigate, Navigate } from 'react-router-dom';

import Swal from 'sweetalert2';

export default function Login(){

	// Allows us to consume the User Context object and it's properties to use for user validation
	const { user, setUser } = useContext(UserContext);

	// hook return a function that lets you navigate to components.
	//const navigate = useNavigate();

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const [isActive, setIsActive] = useState("true")

	function authenticate(e) {

		e.preventDefault();

		// "fetch" method is used to send request in the server and load the received response in the webpage
		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},

			// JSON.stringify converts object data into stringified JSON
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		// The first ".then" method from the "response" object to convert the data retrieved into JSON format to be used in our application
		.then(res => res.json())
		.then(data => {
			// We will receive either a token or an error response.
			console.log(data)
			// If no user information is found, the ' access' property will not be available and will return undefined
			// Using the typeof operator will return a string of the data type of the variable/expression if preseeds which is why the value being compared is ins a string data type
			if(typeof data.access !== "undefined"){

				// The JWT will be used to retrieved user information across the whole frontend application and string it in the localStorage will allow ease of access to the user's information.
				localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access);

				Swal.fire({
					title:'Login successful',
					icon: 'success',
					text: 'Wecome to Shoes Mio'
				})
			} else {
				Swal.fire({
					title: "Authentication Failed!",
					icon: 'error',
					text: 'Please, check your login details and try again!'
				})
			}
		}); 

		

		setEmail("");
		setPassword("");
		
	};

	const retrieveUserDetails = (token) => {

		
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			// Global user state for validation across the whole app
			// Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			});
		});
	};



	useEffect(() => {
		if(email !== "" && password !== ""){
			setIsActive(true)
		}else {
			setIsActive(false)
		}
	}, [email, password]);

	return (
		(user.id !== null) ? 
			<Navigate to="/products" />
		:

		<Form onSubmit={(e) => authenticate(e)}>
			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email here"
					value={email}
					onChange={(e) => setEmail(e.target.value)}
					required
				/>
			</Form.Group>
			<Form.Group controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Password"
					value={password}
					onChange={(e) => setPassword(e.target.value)}
					required
				/>
			</Form.Group>
			{ isActive ? 

				<Button variant="primary my-3" type="submit" id="submitBtn">Submit</Button>

				: 

				<Button variant="danger my-3" type="submit" id="submitBtn" disabled>Submit</Button>

			}
			
		</Form>
	)
}
